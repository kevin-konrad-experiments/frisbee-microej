package frisbee.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import ej.components.dependencyinjection.ServiceLoaderFactory;
import ej.wadapps.storage.Storage;

/**
 * The images used in the application.
 */
public class LocalStorageService {
	/**
	 * Key used for difficulty setting local sotrage
	 */
	public static final String KEY_SETTINGS_DIFFICULTY = "settings_difficulty"; //$NON-NLS-1$

	/**
	 * Key used for bonus items activation setting local sotrage
	 */
	public static final String KEY_SETTINGS_BONUS_ITEMS_ENABLED = "settings_bonus_items_enabled"; //$NON-NLS-1$

	/**
	 * Key used for moving targets activation setting local sotrage
	 */
	public static final String KEY_SETTINGS_MOVING_TARGET_ENABLED = "settings_moving_target_enabled"; //$NON-NLS-1$

	/**
	 * Key used for scores local sotrage
	 */
	public static final String KEY_SCORES = "scores"; //$NON-NLS-1$

	/**
	 * Prevents instantiation
	 */
	private LocalStorageService() {
	}

	/**
	 * @param key
	 *            key of the stored data
	 * @param value
	 *            value of the stored data
	 */
	public static void store(String key, String value) {
		Storage storage = ServiceLoaderFactory.getServiceLoader().getService(Storage.class);

		if (storage == null) {
			System.out.println("Storage unavailable."); //$NON-NLS-1$
			return;
		}

		try (ByteArrayInputStream bais = new ByteArrayInputStream(value.getBytes())) {
			storage.store(key, bais);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param key
	 *            key of the data
	 * @return data
	 */
	public static String load(String key) {
		Storage storage = ServiceLoaderFactory.getServiceLoader().getService(Storage.class);
		StringBuffer buffer = new StringBuffer();

		if (storage == null) {
			System.out.println("Storage unavailable."); //$NON-NLS-1$
			return null;
		}

		try (InputStream stream = storage.load(key)) {
			if (stream == null) {
				return null;
			}

			int byteReaded;
			while ((byteReaded = stream.read()) != -1) {
				buffer.append((char) byteReaded);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return buffer.toString();
	}
}
