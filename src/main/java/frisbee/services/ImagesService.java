package frisbee.services;

import java.io.IOException;

import ej.microui.display.Image;

/**
 * The images used in the application.
 */
public class ImagesService {
	/**
	 * Exit button image
	 */
	public static Image EXIT;

	/**
	 * Game background image beach
	 */
	public static Image BACKGROUND_BEACH;

	/**
	 * Game background image desert
	 */
	public static Image BACKGROUND_DESERT;

	/**
	 * Game mission image beach
	 */
	public static Image MISSION_BEACH;

	/**
	 * Game mission image desert
	 */
	public static Image MISSION_DESERT;

	/**
	 * Game bonus item image
	 */
	public static Image ITEM;

	/**
	 * Frisbee image
	 */
	public static Image FRISBEE;

	/**
	 * Game basic target image
	 */
	public static Image TARGET;

	/**
	 * Game seagull target image
	 */
	public static Image TARGET_SEAGULL;

	/**
	 * Game basic target image
	 */
	public static Image TARGET_SEAGULL_FLIP;

	/**
	 * Game terrorist target image
	 */
	public static Image TARGET_TERRORIST;

	/**
	 * Game terrorist target image
	 */
	public static Image TARGET_TERRORIST_FLIP;

	/**
	 * Game tumbleweed target image
	 */
	public static Image TARGET_TUMBLEWEED;

	/**
	 * Prevents instantiation
	 */
	private ImagesService() {
	}

	/**
	 *
	 */
	public static void initialize() {
		try {
			EXIT = Image.createImage("/images/exit.png"); //$NON-NLS-1$
			BACKGROUND_BEACH = Image.createImage("/images/landscape_beach.png"); //$NON-NLS-1$
			BACKGROUND_DESERT = Image.createImage("/images/landscape_desert.png"); //$NON-NLS-1$
			MISSION_BEACH = Image.createImage("/images/mission_beach.png"); //$NON-NLS-1$
			MISSION_DESERT = Image.createImage("/images/mission_desert.png"); //$NON-NLS-1$
			ITEM = Image.createImage("/images/target.png"); //$NON-NLS-1$
			FRISBEE = Image.createImage("/images/frisbee.png"); //$NON-NLS-1$
			TARGET = Image.createImage("/images/target.png"); //$NON-NLS-1$
			TARGET_SEAGULL = Image.createImage("/images/target_seagull.png"); //$NON-NLS-1$
			TARGET_SEAGULL_FLIP = Image.createImage("/images/target_seagull_flip.png"); //$NON-NLS-1$
			TARGET_TERRORIST = Image.createImage("/images/target_terrorist.png"); //$NON-NLS-1$
			TARGET_TERRORIST_FLIP = Image.createImage("/images/target_terrorist_flip.png"); //$NON-NLS-1$
			TARGET_TUMBLEWEED = Image.createImage("/images/target_tumbleweed.png"); //$NON-NLS-1$
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
