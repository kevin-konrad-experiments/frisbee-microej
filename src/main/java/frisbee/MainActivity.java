package frisbee;

import ej.components.dependencyinjection.ServiceLoaderFactory;
import ej.exit.ExitHandler;
import ej.microui.MicroUI;
import ej.mwt.Desktop;
import ej.mwt.MWT;
import ej.mwt.Panel;
import ej.wadapps.app.Activity;
import ej.widget.container.transition.SlideScreenshotTransitionContainer;
import ej.widget.container.transition.TransitionContainer;
import ej.widget.navigation.page.Page;
import frisbee.pages.MainPage;
import frisbee.services.ImagesService;
import frisbee.services.ScoresService;
import frisbee.services.SettingsService;
import frisbee.services.StylesheetService;

/**
 *
 */
public class MainActivity implements Activity {
	static TransitionContainer transition;

	@Override
	public String getID() {
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onRestart() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStart() {
		MicroUI.start();
		StylesheetService.initialize();
		ImagesService.initialize();
		SettingsService.initialize();
		ScoresService.initialize();

		Panel panel = new Panel();
		Desktop desktop = new Desktop();
		transition = new SlideScreenshotTransitionContainer(MWT.LEFT, false, false);
		home();
		panel.setWidget(transition);
		panel.showFullScreen(desktop);
		desktop.show();
	}

	/**
	 * Go to home page
	 */
	public static void home() {
		transition.show(new MainPage(), false);
	}

	/**
	 * @param page
	 *            new page to show
	 * @param toRight
	 *            transition left to right ?
	 */
	public static void show(Page page, boolean toRight) {
		transition.show(page, toRight);
	}

	/**
	 * Exit progam
	 */
	public static void exit() {
		ExitHandler exitHandler = ServiceLoaderFactory.getServiceLoader().getService(ExitHandler.class);
		if (exitHandler != null) {
			exitHandler.exit();
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
	}
}
