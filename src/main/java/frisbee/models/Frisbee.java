package frisbee.models;

import ej.microui.display.Image;
import frisbee.services.ImagesService;

/**
 *
 */
public class Frisbee {
	/**
	 * X position
	 */
	public int x;

	/**
	 * Y position
	 */
	public int y;

	/**
	 * Z position (not used)
	 */
	public int z;

	/**
	 * Time duration before frisbee is deleted
	 */
	public int duration;

	/**
	 * Angle of the frisbee launch
	 */
	public double angle;

	/**
	 * Speed of the frisbee launch
	 */
	public double speed;

	/**
	 * Intensity of the rotation
	 */
	public double lifting;

	/**
	 * Frisbee has been selected by user to launch ?
	 */
	public boolean pressed;

	/**
	 * Frisbee has been launched ?
	 */
	public boolean launched;

	/**
	 * Image of the frisbee
	 */
	public Image image;

	/**
	 * Constructor
	 */
	public Frisbee() {
		this.x = 250;
		this.y = 250;
		this.z = 250;
		this.duration = 60;
		this.angle = 0;
		this.speed = 0;
		this.lifting = 0;
		this.pressed = false;
		this.launched = false;
		this.image = ImagesService.FRISBEE;
	}
}
