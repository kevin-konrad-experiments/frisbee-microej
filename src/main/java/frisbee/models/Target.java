package frisbee.models;

import java.util.Random;

import ej.microui.display.Image;
import frisbee.services.SettingsService;

/**
 *
 */
public class Target {
	/**
	 * X position
	 */
	public int x;

	/**
	 * Y position
	 */
	public int y;

	/**
	 * Z position (not used)
	 */
	public int z;

	/**
	 * Time duration before target is deleted
	 */
	public int duration;

	/**
	 * Remaining health (not used)
	 */
	public int health;

	/**
	 * Speed of a moving target
	 */
	public int speed;

	/**
	 * Axis 'x' or 'y' of a moving target
	 */
	public String movementAxis;

	/**
	 * Direction '-1', '0' or '1' of a moving target
	 */
	public int movementDirection;

	/**
	 * Score rewarded for shooting the taret
	 */
	public int reward;

	/**
	 * Image of the target
	 */
	public Image image;

	/**
	 * Constructor
	 *
	 * @param image
	 *            image of the target
	 */
	public Target(Image image) {
		super();

		Random random = new Random();
		int randomX = random.nextInt(480) + 10;
		int randomZ = random.nextInt(30) + 5;
		int randomY = (int) (200 - Math.pow(randomZ / 5, 2) * 4);
		int randomDuration = random.nextInt(80) + 20;
		int randomSpeed = random.nextInt(4) + 1;
		int randomHealth = random.nextInt(1) + 1;
		String randomAxis = random.nextBoolean() ? SettingsService.MOVING_AXIS_X : SettingsService.MOVING_AXIS_Y;
		int randomDirection = 0;

		if (SettingsService.TARGETS_MOVING_ENABLED) {
			if (randomAxis == SettingsService.MOVING_AXIS_X) {
				randomDirection = randomX < 240 ? 1 : -1;
			} else if (randomAxis == SettingsService.MOVING_AXIS_Y) {
				randomDirection = randomY < 136 ? 1 : -1;
			}
		}

		this.x = randomX;
		this.y = randomY;
		this.z = randomZ;
		this.duration = randomDuration;
		this.health = randomHealth;
		this.speed = randomSpeed;
		this.movementAxis = randomAxis;
		this.movementDirection = randomDirection;
		this.reward = 50;
		this.image = image;
	}
}
