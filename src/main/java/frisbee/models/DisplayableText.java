package frisbee.models;

import frisbee.services.SettingsService;

/**
 *
 */
public class DisplayableText {
	/**
	 * X position
	 */
	public int x;

	/**
	 * Y position
	 */
	public int y;

	/**
	 * Z position (not used)
	 */
	public int z;

	/**
	 * Time duration before target is deleted
	 */
	public int duration;

	/**
	 * Speed of a moving target
	 */
	public int speed;

	/**
	 * Axis 'x' or 'y' of a moving target
	 */
	public String movementAxis;

	/**
	 * Direction '-1', '0' or '1' of a moving target
	 */
	public int movementDirection;

	/**
	 * Displayed text
	 */
	public String text;

	/**
	 * Constructor
	 *
	 * @param x
	 *            initial x position of the text
	 * @param y
	 *            initial y position of the text
	 * @param text
	 *            displayed text
	 *
	 */
	public DisplayableText(int x, int y, String text) {
		super();

		this.x = x;
		this.y = y;
		this.z = 0;
		this.duration = 20;
		this.speed = 4;
		this.movementAxis = SettingsService.MOVING_AXIS_X;
		this.movementDirection = -1;
		this.text = text;
	}
}
