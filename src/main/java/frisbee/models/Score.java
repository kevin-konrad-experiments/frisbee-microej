package frisbee.models;

/**
 *
 */
public class Score implements Comparable<Score> {
	/**
	 * Name of the player
	 */
	public String playerName;

	/**
	 * Score made by the player
	 */
	public int score;

	/**
	 * Difficulty of the game played
	 */
	public int difficulty;

	/**
	 * Name of the selected mission
	 */
	public String missionName;

	/**
	 * Timestamp of the game
	 */
	public long timestamp;

	/**
	 * Constructor
	 *
	 * @param playerName
	 *            name of the player
	 * @param score
	 *            score of the player
	 * @param difficulty
	 *            difficulty of the game
	 * @param missionName
	 *            name of the mission selected
	 * @param timestamp
	 *            timestamp of the score
	 */
	public Score(String playerName, int score, int difficulty, String missionName, long timestamp) {
		this.playerName = playerName;
		this.score = score;
		this.difficulty = difficulty;
		this.missionName = missionName;
		this.timestamp = timestamp;
	}

	@Override
	public int compareTo(Score score) {
		return Integer.valueOf(score.score).compareTo(Integer.valueOf(this.score));
	}
}
