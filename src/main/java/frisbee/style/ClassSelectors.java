package frisbee.style;

/**
 * Class selectors used in the stylesheet.
 */
public interface ClassSelectors {

	/**
	 * The large icon class selector.
	 */
	String LARGE_ICON = "large_icon"; //$NON-NLS-1$

	/**
	 * The title class selector.
	 */
	String TITLE = "title"; //$NON-NLS-1$

	/**
	 * The list item class selector.
	 */
	String LIST_ITEM = "list_item"; //$NON-NLS-1$

	/**
	 * The list item last score class selector.
	 */
	String LIST_ITEM_LAST_SCORE = "list_item_last_score"; //$NON-NLS-1$

	/**
	 * The list item last score class selector.
	 */
	String LIST_ITEM_1ST_SCORE = "list_item_1st_score"; //$NON-NLS-1$

	/**
	 * The list item last score class selector.
	 */
	String LIST_ITEM_2ND_SCORE = "list_item_2nd_score"; //$NON-NLS-1$

	/**
	 * The list item last score class selector.
	 */
	String LIST_ITEM_3RD_SCORE = "list_item_3rd_score"; //$NON-NLS-1$

	/**
	 * The illustrated button class selector.
	 */
	String ILLUSTRATED_BUTTON = "illustrated_button"; //$NON-NLS-1$

	/**
	 * The text title class selector.
	 */
	String TEXT_TITLE = "text_title"; //$NON-NLS-1$

	/**
	 * The multiline class selector.
	 */
	String MULTILINE = "multiline"; //$NON-NLS-1$

	/**
	 * The selection class selector.
	 */
	String CLASS_SELECTOR_SELECTION = "keyboard-text-selection"; //$NON-NLS-1$
	/**
	 * The clear button class selector.
	 */
	String CLASS_SELECTOR_CLEAR_BUTTON = "keyboard-text-clear-button"; //$NON-NLS-1$

	/**
	 * The space class selector.
	 */
	String SPACE_KEY_SELECTOR = "space_key"; //$NON-NLS-1$

	/**
	 * The backspace class selector.
	 */
	String BACKSPACE_KEY_SELECTOR = "backspace_key"; //$NON-NLS-1$

	/**
	 * The shift key inactive class selector.
	 */
	String SHIFT_KEY_INACTIVE_SELECTOR = "shift_key_inactive"; //$NON-NLS-1$

	/**
	 * The shift key active class selector.
	 */
	String SHIFT_KEY_ACTIVE_SELECTOR = "shift_key_active"; //$NON-NLS-1$

	/**
	 * The switch mapping class selector.
	 */
	String SWITCH_MAPPING_KEY_SELECTOR = "switch_mapping_key"; //$NON-NLS-1$

	/**
	 * The special key class selector.
	 */
	String SPECIAL_KEY_SELECTOR = "special_key"; //$NON-NLS-1$

	/**
	 * The satart button class selector.
	 */
	String START_BUTTON = "start_button"; //$NON-NLS-1$

	/**
	 * The start label class selector.
	 */
	String START_LABEL = "start_label"; //$NON-NLS-1$
}
