package frisbee.pages;

import ej.widget.basic.Button;
import ej.widget.basic.Label;
import ej.widget.container.Dock;
import ej.widget.container.List;
import ej.widget.container.Split;
import ej.widget.listener.OnClickListener;
import ej.widget.navigation.page.Page;
import frisbee.MainActivity;
import frisbee.style.ClassSelectors;

/**
 *
 */
public class MainPage extends Page {
	private static final String PAGE_TITLE = "Frisbee Murderer"; //$NON-NLS-1$
	private static final String MENU_NEW_GAME = "New game"; //$NON-NLS-1$
	private static final String MENU_SCORES = "Scores"; //$NON-NLS-1$
	private static final String MENU_SETTINGS = "Settings"; //$NON-NLS-1$
	private static final String MENU_EXIT = "Exit"; //$NON-NLS-1$

	/**
	 * Constructor
	 */
	public MainPage() {
		super();

		// Top bar
		Dock topBar = new Dock();
		Label titleLabel = new Label(PAGE_TITLE);
		titleLabel.addClassSelector(ClassSelectors.TITLE);
		topBar.setCenter(titleLabel);

		// New game button
		Button newGameButton = new Button(MENU_NEW_GAME);
		newGameButton.addClassSelector(ClassSelectors.LIST_ITEM);
		newGameButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.show(new NewGamePlayerPage(null), true);
			}
		});

		// Scores list button
		Button scoresListButton = new Button(MENU_SCORES);
		scoresListButton.addClassSelector(ClassSelectors.LIST_ITEM);
		scoresListButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.show(new ScoresPage(false, null), true);
			}
		});

		// Settings button
		Button settingsButton = new Button(MENU_SETTINGS);
		settingsButton.addClassSelector(ClassSelectors.LIST_ITEM);
		settingsButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.show(new SettingsPage(), true);
			}
		});

		// Exit button
		Button exitButton = new Button(MENU_EXIT);
		exitButton.addClassSelector(ClassSelectors.LIST_ITEM);
		exitButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.exit();
			}
		});

		// Menu list
		List menuList = new List(false);
		menuList.add(newGameButton);
		menuList.add(scoresListButton);
		menuList.add(settingsButton);
		menuList.add(exitButton);

		// Page layout
		Split content = new Split(false, 0.3f);
		content.setFirst(topBar);
		content.setLast(menuList);

		setWidget(content);
	}
}
