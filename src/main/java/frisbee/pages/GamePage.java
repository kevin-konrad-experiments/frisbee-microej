package frisbee.pages;

import ej.widget.navigation.page.Page;
import frisbee.models.Mission;
import frisbee.models.Player;
import frisbee.widgets.GameWidget;

/**
 *
 */
public class GamePage extends Page {
	/**
	 * @param player
	 *            new player
	 * @param mission
	 *            selected mission
	 */
	public GamePage(Player player, Mission mission) {
		GameWidget gameWidget = new GameWidget(player, mission);
		this.add(gameWidget);
	}
}
