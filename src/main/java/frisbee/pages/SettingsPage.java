package frisbee.pages;

import ej.widget.basic.Button;
import ej.widget.basic.Label;
import ej.widget.basic.picto.PictoSlider;
import ej.widget.basic.picto.PictoSwitch;
import ej.widget.composed.Toggle;
import ej.widget.container.List;
import ej.widget.container.Scroll;
import ej.widget.container.Split;
import ej.widget.listener.OnClickListener;
import ej.widget.listener.OnStateChangeListener;
import ej.widget.listener.OnValueChangeListener;
import ej.widget.navigation.page.Page;
import frisbee.MainActivity;
import frisbee.services.LocalStorageService;
import frisbee.services.SettingsService;
import frisbee.style.ClassSelectors;
import frisbee.style.Pictos;

/**
 * Settings page
 */
public class SettingsPage extends Page {
	private static final String PAGE_TITLE = "Settings"; //$NON-NLS-1$
	private static final String SETTINGS_DIFFICULTY = "Choose difficulty : "; //$NON-NLS-1$
	private static final String SETTINGS_BONUS_ITEMS = "Enable bonus items"; //$NON-NLS-1$
	private static final String SETTINGS_TARGETS_MOVING = "Enable moving targets"; //$NON-NLS-1$

	/**
	 * Constructor
	 */
	public SettingsPage() {
		super();

		// Top bar
		Split topBar = new Split(true, 0.1f);
		Label titleLabel = new Label(PAGE_TITLE);
		titleLabel.addClassSelector(ClassSelectors.TITLE);

		Button backButton = new Button(Character.toString(Pictos.BACK));
		backButton.addClassSelector(ClassSelectors.LARGE_ICON);
		backButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				MainActivity.home();
			}
		});

		topBar.setFirst(backButton);
		topBar.setLast(titleLabel);

		// Settings list
		final Label difficultyLabel = new Label(
				SETTINGS_DIFFICULTY + SettingsService.getDifficultyString(SettingsService.CURRENT_DIFFICULTY));
		PictoSlider difficultySlider = new PictoSlider(SettingsService.EASY_DIFFICULTY, SettingsService.HARD_DIFFICULTY,
				SettingsService.CURRENT_DIFFICULTY);
		difficultySlider.addOnValueChangeListener(new OnValueChangeListener() {
			@Override
			public void onValueChange(int newValue) {
				SettingsService.CURRENT_DIFFICULTY = newValue;
				difficultyLabel.setText(
						SETTINGS_DIFFICULTY + SettingsService.getDifficultyString(SettingsService.CURRENT_DIFFICULTY));
				LocalStorageService.store(LocalStorageService.KEY_SETTINGS_DIFFICULTY, String.valueOf(newValue));
			}

			@Override
			public void onMinimumValueChange(int newMinimum) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onMaximumValueChange(int newMaximum) {
				// TODO Auto-generated method stub
			}
		});

		Toggle bonusItems = new Toggle(new PictoSwitch(), SETTINGS_BONUS_ITEMS);
		bonusItems.setChecked(SettingsService.BONUS_ITEMS_ENABLED);
		bonusItems.addOnStateChangeListener(new OnStateChangeListener() {
			@Override
			public void onStateChange(boolean newState) {
				SettingsService.BONUS_ITEMS_ENABLED = newState;
				LocalStorageService.store(LocalStorageService.KEY_SETTINGS_BONUS_ITEMS_ENABLED,
						String.valueOf(newState));
			}
		});

		Toggle movingTargets = new Toggle(new PictoSwitch(), SETTINGS_TARGETS_MOVING);
		movingTargets.setChecked(SettingsService.TARGETS_MOVING_ENABLED);
		movingTargets.addOnStateChangeListener(new OnStateChangeListener() {
			@Override
			public void onStateChange(boolean newState) {
				SettingsService.TARGETS_MOVING_ENABLED = newState;
				LocalStorageService.store(LocalStorageService.KEY_SETTINGS_MOVING_TARGET_ENABLED,
						String.valueOf(newState));
			}
		});

		List settingsList = new List(false);
		settingsList.add(difficultyLabel);
		settingsList.add(difficultySlider);
		settingsList.add(movingTargets);
		settingsList.add(bonusItems);

		// Settings scroll
		Scroll settingsScroll = new Scroll(false, false);
		settingsScroll.setWidget(settingsList);

		// Page layout
		Split content = new Split(false, 0.3f);
		content.setFirst(topBar);
		content.setLast(settingsScroll);

		this.setWidget(content);
	}
}