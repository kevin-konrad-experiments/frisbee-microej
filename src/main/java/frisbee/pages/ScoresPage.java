package frisbee.pages;

import ej.microui.display.GraphicsContext;
import ej.mwt.Widget;
import ej.style.Style;
import ej.style.container.Rectangle;
import ej.widget.basic.Button;
import ej.widget.basic.Label;
import ej.widget.container.Grid;
import ej.widget.container.List;
import ej.widget.container.Scroll;
import ej.widget.container.Split;
import ej.widget.listener.OnClickListener;
import ej.widget.navigation.page.Page;
import frisbee.MainActivity;
import frisbee.models.Score;
import frisbee.services.ScoresService;
import frisbee.services.SettingsService;
import frisbee.style.ClassSelectors;
import frisbee.style.Pictos;

/**
 *
 */
public class ScoresPage extends Page {
	private static final String PAGE_TITLE = "Scores"; //$NON-NLS-1$
	private final Scroll scoresScroll;
	private final List scoresList;
	private boolean backFromGame = false;

	/**
	 * Constructor
	 *
	 * @param backToNewGame
	 *            should back button go to new game page?
	 * @param playerName
	 *            last player name entered
	 */
	public ScoresPage(final boolean backToNewGame, final String playerName) {
		super();
		this.backFromGame = backToNewGame;

		// Top bar
		Split topBar = new Split(true, 0.1f);
		Label titleLabel = new Label(PAGE_TITLE);
		titleLabel.addClassSelector(ClassSelectors.TITLE);

		Button backButton = new Button(Character.toString(Pictos.BACK));
		backButton.addClassSelector(ClassSelectors.LARGE_ICON);
		backButton.addOnClickListener(new OnClickListener() {
			@Override
			public void onClick() {
				if (backToNewGame) {
					MainActivity.show(new NewGamePlayerPage(playerName), false);
				} else {
					MainActivity.home();
				}
			}
		});

		topBar.setFirst(backButton);
		topBar.setLast(titleLabel);

		// Scores list
		this.scoresList = new List(false);
		int rank = 1;
		for (Score score : ScoresService.getScores(true)) {
			Grid scoreGrid = new Grid(true, 5);

			if (this.backFromGame && score == ScoresService.getLastScoreAdded()) {
				scoreGrid.addClassSelector(ClassSelectors.LIST_ITEM_LAST_SCORE);
			} else {
				switch (rank) {
				case 1:
					scoreGrid.addClassSelector(ClassSelectors.LIST_ITEM_1ST_SCORE);
					break;
				case 2:
					scoreGrid.addClassSelector(ClassSelectors.LIST_ITEM_2ND_SCORE);
					break;
				case 3:
					scoreGrid.addClassSelector(ClassSelectors.LIST_ITEM_3RD_SCORE);
					break;
				}
			}

			scoreGrid.add(new Label(String.valueOf(rank)));
			scoreGrid.add(new Label(score.playerName));
			scoreGrid.add(new Label(String.valueOf(score.score)));
			scoreGrid.add(new Label(SettingsService.getDifficultyString(score.difficulty)));
			scoreGrid.add(new Label(score.missionName));
			this.scoresList.add(scoreGrid);
			rank++;
		}

		// Scores scroll
		this.scoresScroll = new Scroll(false, false);
		this.scoresScroll.setWidget(this.scoresList);

		// Page layout
		Split content = new Split(false, 0.3f);
		content.setFirst(topBar);
		content.setLast(this.scoresScroll);

		this.setWidget(content);
	}

	@Override
	public void renderContent(GraphicsContext g, Style style, Rectangle bounds) {
		super.renderContent(g, style, bounds);

		if (this.backFromGame) {
			int lastScoreIndex = ScoresService.getLastScoreIndex();
			Widget lastScoreWidget = this.scoresList.getWidget(lastScoreIndex);
			this.scoresScroll.scrollTo(lastScoreWidget.getY(), true);
		}
	}
}
